# Packer AMI builder

A collection of Packer files for the creation of Amazon Machine Images (AMIs) on AWS.

AMIs list:
* GitLab-EE-Password-Set - A current version of Enterprise EE (AMD64) with the root password set to `gitlab1234`. Useful for scripting/Terraform
* Docker-CE - A community version of Docker installed on top of Ubuntu AMD64.
* Dcoker-CE-ARM64 - A community version of Docker installed on top of Ubuntu ARM64.
* GitLab-Runner-Dedicated-Docker - A dedicated EC2 AMI *with* Docker (AMD64) installed. Image depends on Docker-CE image (AMD64).
* GitLab-Runner-Docker-ARM64 - A dedicated EC2 AMI *with* Docker (ARM64) installed. Image depends on Docker-CE image (ARM64).
* GitLab-Runner-Dedicated-No-Docker - A dedicated EC2 (AMD64) AMI sans Docker.
* OpenLDAP - OpenLDAP (AMD64) AMI w/ sample data.
* Node - Creates Node.js 12.6.1 (AMD64) image. 

**note** All images are based on Ubuntu 18.04. The SSH username to access the images is `ubuntu`

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

If you want to build in your local environment then you will need to install [HashiCorp Packer](https://www.packer.io/downloads.html)

The scripts assume you have exported your AWS programatic credentials. The format for exporting is:

* ``export AWS_ACCESS_KEY_ID="yourkeyid"``
* ``export AWS_SECRET_ACCESS_KEY="yoursecretid"``

### Creating AWS AMIs

Local test can be run by issuing the `packer build` command: 

``packer build ./<directory>/*.json``

For example, to a build GitLab Enterprise Edition the command would (from root of clone):

``packer build ./GitLab-EE-Password-Set/packer-GitLab-EE-Password-Set.json``

### CI/CD configuration

Valid AWS credentials are needed.  If you fork or clone this project to GitLab then navigate, in a web browser, to Settings -> CI/CD -> Variables. 

Two variables to create are: 
1. AWS_ACCESS_KEY_ID 
1. AWS_SECRET_ACCESS_KEY

It is also suggested that you mask these variables to ensure any troubleshooting does not expose the credentials on the Internet.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

