#!/bin/bash

# Uncomment the lines below if you are not using a recently built Docker host. It's redundant in the project but adding as it's a personal best practice
# export DEBIAN_FRONTEND=noninteractive 
# sudo apt update -y
# sudo apt upgrade -yq
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_arm64.deb
sudo dpkg -i gitlab-runner_arm64.deb
