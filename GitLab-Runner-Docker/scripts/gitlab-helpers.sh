#!/bin/bash


docker run -d -p 6000:5000 \
    -e REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io \
    --restart always \
    --name registry registry:2

docker run -d -p 9005:9000 \
    -v /.minio:/root/.minio -v /export:/export \
    --name minio \
    --restart always \
    minio/minio:latest server /export

sudo mkdir /export/runner

# Docker may take a second to respond. Future - put
sleep 2
